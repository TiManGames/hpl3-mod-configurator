import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import org.fg.soma.manager.models.mod.Mod;
import org.fg.soma.manager.util.XmlDocument;
import org.junit.Test;
import org.testfx.util.WaitForAsyncUtils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.testfx.api.FxAssert.verifyThat;

public class RegressionTests extends TestFXBase {
    @Test
    public void A() { // verifyGamePathEnablesModProfilesTable
        // Select mod folder
        clickOn("#chooseFolderBtn");
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection stringSelection = new StringSelection(TEST_GAME_PATH);
        clipboard.setContents(stringSelection, stringSelection);
        press(KeyCode.CONTROL)
                .press(KeyCode.V)
                .release(KeyCode.V)
                .release(KeyCode.CONTROL);
        push(KeyCode.ENTER)
                .push(KeyCode.ENTER);

        verifyThat("#modProfilesTable", (TableView<Mod> modProfilesTable)
                -> !modProfilesTable.isDisabled());
    }

    @Test
    public void B() { // verifyNewModIsAddedToModProfilesTable
        clickOn("#newModBtn");

        TableView<Mod> modProfilesTable = find("#modProfilesTable");
        assertEquals(modProfilesTable.getItems().size(), 1);

        Mod newMod = modProfilesTable.getItems().get(0);

        verifyThat("#modProfilesTable", e ->
                newMod.getModName().equals("New mod"));
    }

    @Test
    public void C() { // verifyModProfilesRowEnablesModInfoGrid
        Node node = lookup("#modNameCol").nth(1).query();
        clickOn(node);

        WaitForAsyncUtils.waitForFxEvents();

        verifyThat("#modInfoGrid", (GridPane modInfoGrid)
                -> !modInfoGrid.isDisabled());
    }

    @Test
    public void D() { // verifyNewModInfoAppearsInGird
        Mod newMod = getModFromTable();

        verifyThat("#modInfoNameField", (javafx.scene.control.TextField modInfoField)
                -> modInfoField.getText().contains(newMod.getModName()));
        verifyThat("#modInfoAuthorField", (javafx.scene.control.TextField modAuthorField)
                -> modAuthorField.getText().contains(newMod.getModAuthor()));
        verifyThat("#modDescriptionArea", (javafx.scene.control.TextArea modDescription)
                -> modDescription.getText().contains("Description"));
    }

    @Test
    public void E() { // verifyApplyButtonUpdatesApplicationConfigFile
        addModDependency();

        XmlDocument appConfigDoc = new XmlDocument(new File("modManager.cfg"));
        assertEquals(appConfigDoc.getDocument().getElementsByTagName("Mod").getLength(), 1);
    }

    @Test
    public void F() { // verifyModEntryFilePhysicallyExists
        File modEntryFile = new File(TEST_MOD_FILE);
        assertTrue(Files.exists(Paths.get(modEntryFile.getAbsolutePath())));
    }

    @Test
    public void G() { // verifyModEntryFileDataIsCorrect
        File modEntryFile = new File(TEST_MOD_FILE);

        XmlDocument modDoc = new XmlDocument(modEntryFile);
        assertEquals("new.dependency", modDoc.getRootAttribute("Dependencies"));
        assertEquals("New mod", modDoc.getRootAttribute("Title"));
        assertEquals("Author", modDoc.getRootAttribute("Author"));
        assertEquals("mod.uid", modDoc.getRootAttribute("UID"));
        assertEquals("new.dependency", modDoc.getRootAttribute("Dependencies"));
    }

    private void addModDependency() {
        clickOn("#modDependenciesBtn");

        TextField textField = find("#uidField");
        textField.setText("mod.uid");

        clickOn("#addDepBtn");
        clickOn("#saveDepBtn");
        clickOn("#applyBtn");
    }

    private Mod getModFromTable() {
        TableView<Mod> modProfilesTable = find("#modProfilesTable");

        return modProfilesTable.getItems().get(0);
    }
}
