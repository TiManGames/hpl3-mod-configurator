import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.fg.soma.manager.ModManagerApplication;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;

import java.io.File;
import java.util.concurrent.TimeoutException;

public abstract class TestFXBase extends ApplicationTest {
    protected final static String TEST_GAME_PATH = "D:\\SteamLibrary\\steamapps\\common\\SOMA";
    protected final static String TEST_MOD_FILE = String.format("%s\\mods\\Newmod\\entry.hpc", TEST_GAME_PATH);

    @BeforeClass
    public static void setup() throws Exception {
        File configFile = new File("modManager.cfg");
        if (configFile.exists()) {
            FileUtils.forceDelete(configFile);
        }

        File modDir = new File(String.format("%s\\mods\\Newmod", TEST_GAME_PATH));
        if (modDir.exists()) {
            FileUtils.forceDelete(modDir);
        }

        ApplicationTest.launch(ModManagerApplication.class);
    }

    @Override
    public void start(Stage stage) {
        stage.show();
    }

    @After
    public void afterEachTest() {
        release(new KeyCode[] {});
        release(new MouseButton[] {});
    }

    @AfterClass
    public static void afterClass() throws TimeoutException {
        FxToolkit.hideStage();
    }

    /* Just a shortcut to retrieve widgets in the GUI. */
    public <T extends Node> T find(final String query) {
        return lookup(query).query();
    }
}
