package org.fg.soma.manager.models.mod;

import org.fg.soma.manager.util.BatchWriter;
import org.fg.soma.manager.util.XmlDocument;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents an HPL3 mod entry file (.hpc) and the data inside it. <br> <br>
 * Note that a mod can either be loaded from an existing path or by creating a new mod object which will later
 * be manually saved by the end user. <br> <br>
 * Dependencies and thumbnail may be manually added by the end user and are not included inside a minimal mod object.
 */
public final class Mod {
    private final List<ModDependency> modDependencies = new ArrayList<>();

    private Path location;
    private ModType modType;
    private File modImage;
    private String modName;
    private String modAuthor;
    private String modDescription;
    private String modUid;

    private Mod() {
    }

    /**
     * Creates a basic mod object with the essential, minimal properties.<br>
     * The mod object is created regardless if a corresponding mod file (.hpc) exists, this is taken care of
     * when the end user manually presses the <code>Apply</code> button after the mod object is created.<br><br>
     * The location of the mod is always a new folder inside the game's <code>/mods</code> folder.
     * The name of the mod's parent folder is generated from the mod's name without any potential spaces.
     *
     * @param name        The name of the mod.
     * @param author      The author of the mod.
     * @param description The description of the mod.
     * @param path        The path to the physical location of the .hpc file.
     */
    public Mod(String name, String author, String description, String path) {
        this.modName = name;
        this.modAuthor = author;
        this.modDescription = description;
        this.modType = ModType.STANDALONE;

        String defaultLocation = path + "/mods/" +
                this.modName.replace(" ", "") + "/entry.hpc";

        this.location = Paths.get(defaultLocation);
    }

    /**
     * Creates a mod object from an existing .hpc file. The data gets parsed from the .hpc file.
     *
     * @param location The location of the physical .hpc file.
     */
    public Mod(Path location) {
        XmlDocument entry = new XmlDocument(location.toFile());

        String[] dependencies = entry.getRootAttribute("Dependencies").split(",");

        String title = entry.getRootAttribute("Title");
        String author = entry.getRootAttribute("Author");
        String description = entry.getRootAttribute("Description");
        String type = entry.getRootAttribute("Type");
        String picFileName = entry.getRootAttribute("LauncherPic");
        String uid = entry.getRootAttribute("UID");

        File launcherPic = picFileName.isEmpty() ? null : new File(String.format("%s/%s", location.getParent(), picFileName));
        ModType resultedType = type.equals("StandAlone") ? ModType.STANDALONE : ModType.ADDON;

        for (String dep : dependencies) {
            this.modDependencies.add(new ModDependency(dep));
        }

        this.location = location;
        this.modName = title;
        this.modAuthor = author;
        this.modDescription = description;
        this.modType = resultedType;
        this.modImage = launcherPic;
        this.modUid = uid;
    }

    /**
     * Adds a dependency object to this mod.
     *
     * @param dependency The dependency to add.
     */
    public void addDependency(ModDependency dependency) {
        this.modDependencies.add(dependency);
    }

    /**
     * Adds description to this mod.
     *
     * @param modDescription The description to add.
     */
    public void setDescription(String modDescription) {
        this.modDescription = modDescription;
    }

    /**
     * Gets the mod location (.hpc file).
     *
     * @return A {@link Path} representation of the mod's .hpc file.
     */
    public Path getLocation() {
        return this.location;
    }

    /**
     * Gets the mod's type: Dependency or AddOn.
     *
     * @return The {@link ModType} of the mod.
     */
    public ModType getModType() {
        return this.modType;
    }

    /**
     * Set the mod's {@link ModType}. It is used by the game's mod launcher to determine where to store the mod.
     *
     * @param modType The type to set.
     */
    public void setModType(ModType modType) {
        this.modType = modType;
    }

    public void setModImage(File modImage) {
        this.modImage = modImage;
    }

    public String getModImageName() {
        return this.modImage == null ? "" : this.modImage.getName();
    }

    public String getModName() {
        return this.modName;
    }

    public void setModName(String modName) {
        this.modName = modName;
    }

    public String getModAuthor() {
        return this.modAuthor;
    }

    public void setModAuthor(String modAuthor) {
        this.modAuthor = modAuthor;
    }

    public String getModDescription() {
        return this.modDescription;
    }

    public String getUID() {
        return this.modUid;
    }

    public void setUID(String uid) {
        this.modUid = uid;
    }

    public List<ModDependency> getDependencies() {
        return this.modDependencies;
    }

    /**
     * Generates a batch (.bat) file that can launch the mod via the debug mode of the game.<br>
     * The file will always be generated at the base game folder.
     */
    public void generateDevFile() {
        String baseName = "ModDev_";
        String fileName = baseName + modName.replaceAll("[$&+,:;=\\\\?@#|/'<>.^*()%!-]", "");
        String modPath = location.toString();
        String gamePath = location.getParent() // Mod folder
                .getParent() // Root mods folder
                .getParent() // Game folder
                .toString();

        String content = "Soma.exe -user Dev -cfg config/main_init_dev.cfg " +
                "-mod \"" + modPath;

        BatchWriter.writeFile(fileName, content, gamePath);
    }

    /**
     * Compares two mods by comparing their {@link Path} properties.
     *
     * @param other The mod to compare to this mod.
     * @return zero if the mod argument's path is {@link #equals equal} to this mod's path, a
     * value less than zero if this mod's path is lexicographically less than
     * the argument, or a value greater than zero if this mod's path is
     * lexicographically greater than the argument.
     */
    public int compareTo(Mod other) {
        return this.location.compareTo(other.getLocation());
    }
}
