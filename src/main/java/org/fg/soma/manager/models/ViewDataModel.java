package org.fg.soma.manager.models;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import org.fg.soma.manager.models.mod.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * This class is responsible for transferring data between controllers in the application,
 * including {@link StringProperty}, {@link BooleanProperty}
 * and other properties of different UI components.<br>
 * That might be updated from one or more controllers at a time.<br><br>
 *
 * Do <b>not</b> inject any controller into another controller. Instead, add your desired property to the this model
 * And bind it to the appropriate JavaFX property in the controller.
 *
 * Example:<br><br>
 * <code>
 *      public void initialize(URL location, ResourceBundle resources) {
 *      textField.textProperty().bindBidirectional(viewDataModel.textFieldProperty());
 * </code>
 */
@Service
@Scope(BeanDefinition.SCOPE_SINGLETON)
public class ViewDataModel {
    private final Property<ObservableList<Mod>> modProfilesProperty;
    private final BooleanProperty modProfilesDisableProperty;

    private final StringProperty gamePathProperty;
    private final StringProperty modNameProperty;
    private final StringProperty modAuthorProperty;
    private final StringProperty modDescriptionProperty;
    private final StringProperty modThumbnailProperty;
    private final StringProperty modTypeProperty;

    private TableView<Mod> modProfilesTable;

    @Autowired
    public ViewDataModel() {
        this.modProfilesProperty = new SimpleObjectProperty<>();
        this.modProfilesDisableProperty = new SimpleBooleanProperty();

        this.gamePathProperty = new SimpleStringProperty();
        this.modNameProperty = new SimpleStringProperty();
        this.modAuthorProperty = new SimpleStringProperty();
        this.modDescriptionProperty = new SimpleStringProperty();
        this.modThumbnailProperty = new SimpleStringProperty();
        this.modTypeProperty = new SimpleStringProperty();

        this.modProfilesTable = new TableView<>();
    }

    public void bindModProfilesTable(TableView<Mod> table) {
        this.modProfilesTable = table;
    }

    public Property<ObservableList<Mod>> modProfilesProperty() {
        return modProfilesProperty;
    }

    public BooleanProperty modProfilesDisableProperty() {
        return modProfilesDisableProperty;
    }

    public StringProperty gamePathProperty() {
        return gamePathProperty;
    }

    public StringProperty modNameProperty() {
        return modNameProperty;
    }

    public StringProperty modAuthorProperty() {
        return modAuthorProperty;
    }

    public StringProperty modDescriptionProperty() {
        return modDescriptionProperty;
    }

    public StringProperty modThumbnailProperty() {
        return modThumbnailProperty;
    }

    public TableView<Mod> modProfilesTable() {
        return modProfilesTable;
    }

    public StringProperty modTypeProperty() {
        return modTypeProperty;
    }
}
