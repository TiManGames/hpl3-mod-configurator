package org.fg.soma.manager.models.mod;

import javafx.collections.ObservableList;
import org.fg.soma.manager.models.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class ModProfilesManager {
    private final ApplicationConfig applicationConfig;
    private final ObservableList<Mod> modProfiles;

    private Mod selectedMod;

    @Autowired
    public ModProfilesManager(ApplicationConfig applicationConfig) {
        this.applicationConfig = applicationConfig;
        this.modProfiles = applicationConfig.loadMods();
    }

    public ObservableList<Mod> getModProfiles() {
        return modProfiles;
    }

    public Mod getSelectedMod() {
        return selectedMod;
    }

    public void setSelectedMod(Mod mod) {
        this.selectedMod = mod;
    }

    /**
     * Adds a mod to the profile manager's observable mod list. Will not be added if the mod has already been added
     * before.
     *
     * @param mod The mod to add.
     * @return If the mod has been added or not.
     */
    public boolean addMod(Mod mod) {
        boolean add = true;

        for (Mod addedMod : modProfiles) {
            if (addedMod.compareTo(mod) == 0) {
                add = false;
                break;
            }
        }

        if (add) {
            modProfiles.add(mod);
            applicationConfig.saveMod(mod);
        }

        return add;
    }

    /**
     * Removes a mod from the profile manager's observable mod list.
     *
     * @param mod The mod to remove.
     */
    public void removeMod(Mod mod) {
        if (modProfiles.stream().anyMatch(modToRemove -> modToRemove.compareTo(mod) == 0)) {
            modProfiles.remove(mod);
            applicationConfig.deleteMod(mod);
        }
    }

    public Mod getModByPath(Path path) {
        return modProfiles.stream()
                .filter(mod -> mod.getLocation().compareTo(path) == 0)
                .findFirst()
                .orElse(null);
    }
}
