package org.fg.soma.manager.models.mod;

public enum ModType {
    STANDALONE("StandAlone"),
    ADDON("AddOn");

    private final String typeName;

    ModType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return this.typeName;
    }
}
