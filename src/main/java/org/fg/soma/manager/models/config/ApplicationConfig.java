package org.fg.soma.manager.models.config;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import org.fg.soma.manager.config.ApplicationTheme;
import org.fg.soma.manager.models.mod.Mod;
import org.fg.soma.manager.util.XmlDocument;
import org.fg.soma.manager.util.javafx.AlertMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.fg.soma.manager.util.application.ConfigConstants.CONFIG_APP_ATTR_MOD_LOCATION;
import static org.fg.soma.manager.util.application.ConfigConstants.CONFIG_APP_ELEMENT_GAME_DIR;
import static org.fg.soma.manager.util.application.DialogConstants.*;
import static org.fg.soma.manager.util.application.GlobalConstants.USER_DIRECTORY;

/**
 * Global configuration file (.cfg) for the mod manager application.
 */
@Component
public class ApplicationConfig extends ConfigFile {
    private final ObservableList<Mod> mods = FXCollections.observableArrayList();

    private boolean missingMods;

    @Autowired
    public ApplicationConfig() {
        super(String.format("%s/modManager.cfg", USER_DIRECTORY));
        missingMods = false;
    }

    /**
     * Add and save a mod into the config file.
     *
     * @param mod The mod to add.
     */
    public void saveMod(Mod mod) {
        Element modsContainer = (Element) xmlDocument.getDocument().getElementsByTagName("Mods").item(0);

        Element newMod = xmlDocument.getDocument().createElement("Mod");
        newMod.setAttribute(CONFIG_APP_ATTR_MOD_LOCATION, mod.getLocation().toString());

        modsContainer.appendChild(newMod);
        modsContainer.normalize();

        xmlDocument.save();
    }

    /**
     * Deletes a mod (From the config file) and save.
     *
     * @param mod The mod to remove.
     */
    public void deleteMod(Mod mod) {
        NodeList modsFromConfig = xmlDocument.getDocument().getElementsByTagName("Mod");
        Element modsContainer = xmlDocument.getElementByName("Mods");

        for (int i = modsFromConfig.getLength() - 1; i >= 0; i--) {
            Element element = (Element) modsFromConfig.item(i);
            String location = element.getAttribute(CONFIG_APP_ATTR_MOD_LOCATION);

            if (location.equals(mod.getLocation().toString())) {
                modsContainer.removeChild(element);
            }
        }

        modsContainer.normalize();

        xmlDocument.save();
    }

    /**
     * @return String representation of the game folder path. If no path is available, <code>null</code> is returned.
     */
    public String getGameFolderPath() {
        return xmlDocument.getElementByName(CONFIG_APP_ELEMENT_GAME_DIR)
                .getAttributes()
                .item(0)
                .getNodeValue();
    }

    /**
     * Sets the path of the game folder.
     *
     * @param path The path to set.
     */
    public void setGameFolderPath(String path) {
        Element gameDirElement = xmlDocument.getElementByName(CONFIG_APP_ELEMENT_GAME_DIR);
        gameDirElement.getAttributes().item(0).setNodeValue(path);

        xmlDocument.save();
    }

    /**
     * Sets the color theme of the application.
     *
     * @param theme The color theme to set: Light, Dark or Ocean.
     */
    public void setTheme(ApplicationTheme theme) {
        ApplicationTheme.setSelectedTheme(theme);

        Element colorTheme = xmlDocument.getElementByName("Theme");
        colorTheme.setAttribute("Name", theme.name());

        xmlDocument.save();
    }

    /**
     * @return The application color theme.
     */
    public ApplicationTheme getTheme() {
        Element colorTheme = xmlDocument.getElementByName("Theme");

        return Enum.valueOf(ApplicationTheme.class, colorTheme.getAttribute("Name"));
    }

    /**
     * Gets all the mods from the config file.
     * If there are missing mods, they will be removed from the config file.
     *
     * @return The mods as {@link ObservableList}.
     */
    public ObservableList<Mod> loadMods() {
        NodeList modNodes = xmlDocument.getDocument().getElementsByTagName("Mod");

        for (int i = 0; i < modNodes.getLength(); i++) {
            String loc = modNodes.item(i).getAttributes().getNamedItem(CONFIG_APP_ATTR_MOD_LOCATION).getNodeValue();

            if (Files.exists(Paths.get(loc))) {
                Mod mod = new Mod(Paths.get(loc));
                mods.add(mod);
            } else {
                xmlDocument.getRootElement()
                        .getElementsByTagName("Mods")
                        .item(0)
                        .removeChild(modNodes.item(i));

                xmlDocument.save();

                missingMods = true;
            }
        }

        if (missingMods) {
            Task<Void> task = new Task<Void>() {
                @Override
                public Void call() throws Exception {
                    Thread.sleep(1000);

                    return null;
                }
            };

            task.setOnSucceeded(e -> AlertMaker.info("Loading mod profiles",
                    "Some mods could not be loaded.",
                    "Did you remove the mods or change their location?"));

            new Thread(task).start();
        }

        return mods;
    }

    @Override
    protected void generate() {
        xmlDocument = new XmlDocument(new File(super.configFilePath));
        xmlDocument.setRootElement("ModManagerConfig");

        Element rootElement = xmlDocument.getRootElement();
        xmlDocument.addElement(rootElement, "GameFolder");

        Element gameFolder = xmlDocument.getElementByName("GameFolder");
        gameFolder.setAttribute("Path", "");

        xmlDocument.addElement(rootElement, "Theme");

        Element colorTheme = xmlDocument.getElementByName("Theme");
        colorTheme.setAttribute("Name", ApplicationTheme.selectedTheme().name());

        xmlDocument.addElement(rootElement, "Mods");
        xmlDocument.save();

        if (!isConfigFileExist()) {
            AlertMaker.error(DIALOG_CONFIG_APP_ERROR_TITLE,
                    DIALOG_CONFIG_APP_ERROR_HEADER,
                    DIALOG_CONFIG_APP_ERROR_CONTENT);
        }
    }
}
