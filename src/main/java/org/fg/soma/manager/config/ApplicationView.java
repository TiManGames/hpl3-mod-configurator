package org.fg.soma.manager.config;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

import static org.fg.soma.manager.util.application.GlobalConstants.APPLICATION_TITLE_DEP;
import static org.fg.soma.manager.util.application.GlobalConstants.APPLICATION_TITLE_MAIN;

public enum ApplicationView {
    MOD_MANAGER {
        @Override
        String getTitle() {
            return APPLICATION_TITLE_MAIN;
        }

        @Override
        String getFxmlFilePathName() {
            return "view/MainApplication.fxml";
        }

        @Override
        ApplicationView getOwnerView() {
            return null;
        }

        @Override
        EventHandler<WindowEvent> getOnCloseRequest() {
            return event -> Platform.exit();
        }
    },
    MOD_DEPENDENCIES {
        @Override
        String getTitle() {
            return APPLICATION_TITLE_DEP;
        }

        @Override
        String getFxmlFilePathName() {
            return "view/ModDependencies.fxml";
        }

        @Override
        ApplicationView getOwnerView() {
            return ApplicationView.MOD_MANAGER;
        }

        @Override
        EventHandler<WindowEvent> getOnCloseRequest() {
            return null;
        }
    };

    abstract String getTitle();

    abstract String getFxmlFilePathName();

    abstract ApplicationView getOwnerView();

    abstract EventHandler<WindowEvent> getOnCloseRequest();
}
