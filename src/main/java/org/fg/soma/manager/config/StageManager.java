package org.fg.soma.manager.config;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.fg.soma.manager.models.config.ApplicationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

public class StageManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(StageManager.class);

    @Autowired
    private ApplicationConfig applicationConfig;

    private final HashMap<Enum<ApplicationView>, Stage> stages;
    private final SpringFXMLLoader springFXMLLoader;
    private final Stage primaryStage;

    public StageManager(SpringFXMLLoader springFXMLLoader, Stage primaryStage) {
        this.stages = new HashMap<>();
        this.springFXMLLoader = springFXMLLoader;
        this.primaryStage = primaryStage;
    }

    /**
     * Opens an FXML view.
     *
     * @param view The {@link ApplicationView} to open.
     * @throws IOException If the FXML file loading has failed.
     */
    public void openView(final ApplicationView view) throws IOException {
        Parent viewRootNodeHierarchy = loadViewNodeHierarchy(view.getFxmlFilePathName());
        show(viewRootNodeHierarchy, view);
    }

    /**
     * Closes an FXML view.
     *
     * @param view The {@link ApplicationView} to close.
     */
    public void closeView(final ApplicationView view) {
        stages.get(view).close();
    }

    /**
     * Sets the theme of the application.
     *
     * @param theme The theme to set: Light, Dark or Ocean.
     */
    public void setTheme(final ApplicationTheme theme) {
        stages.get(ApplicationView.MOD_MANAGER).getScene().getStylesheets().set(0, theme.getCss());
    }

    /**
     * Loads the object hierarchy from a FXML document and returns to root node
     * of that hierarchy.
     *
     * @return Parent root node of the FXML document hierarchy
     */
    private Parent loadViewNodeHierarchy(String fxmlFilePathName) {
        Parent rootNode = null;

        try {
            rootNode = springFXMLLoader.load(fxmlFilePathName);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        return rootNode;
    }

    private void show(final Parent rootNode, ApplicationView view) {
        Scene scene = setupScene(rootNode);

        Stage stage = new Stage();
        stage.initOwner(stages.get(view.getOwnerView()));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(scene);
        stage.setTitle(view.getTitle());
        stage.setResizable(false);
        stage.setOnCloseRequest(view.getOnCloseRequest());
        stage.getIcons().add(
                new Image(Objects.requireNonNull(
                        getClass().getClassLoader().getResourceAsStream("icon_windows.png"))));
        stage.centerOnScreen();
        stage.show();

        stages.put(view, stage);
    }

    private Scene setupScene(final Parent rootNode) {
        Scene scene = primaryStage.getScene();

        if (scene == null) {
            scene = new Scene(rootNode);
        }

        scene.setRoot(rootNode);
        scene.getStylesheets().add(0, applicationConfig.getTheme().getCss()); // Set up default theme

        return scene;
    }
}
