package org.fg.soma.manager.config;

public enum ApplicationTheme {
    LIGHT(""),
    DARK("view/dark.css"),
    OCEAN("view/ocean.css");

    private static ApplicationTheme selectedTheme = ApplicationTheme.DARK;
    private final String css;

    ApplicationTheme(String css) {
        this.css = css;
    }

    public String getCss() {
        return this.css;
    }

    public static void setSelectedTheme(ApplicationTheme theme) {
        selectedTheme = theme;
    }

    public static ApplicationTheme selectedTheme() {
        return selectedTheme;
    }
}
