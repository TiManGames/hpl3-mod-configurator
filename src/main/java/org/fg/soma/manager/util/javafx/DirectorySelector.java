package org.fg.soma.manager.util.javafx;

import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;

import static org.fg.soma.manager.util.application.GlobalConstants.USER_HOME;

/**
 * Utility class for handling JavaFX directory choosers.
 */
public final class DirectorySelector {
    private DirectorySelector() {
    }

    /**
     * Displays a directory selector window for the user to select a folder.
     *
     * @param title The title of the window.
     * @return The directory represented as {@link File}.
     */
    public static File selectDirectory(String title) {
        DirectoryChooser directoryChooser = new DirectoryChooser();

        directoryChooser.setTitle(title);
        directoryChooser.setInitialDirectory(new File(USER_HOME));

        return directoryChooser.showDialog(new Stage());
    }
}
