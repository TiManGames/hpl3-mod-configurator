package org.fg.soma.manager.util;

import org.fg.soma.manager.util.javafx.AlertMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.fg.soma.manager.util.application.DialogConstants.*;

/**
 * Utility class for writing batch (.bat) files.
 */
public final class BatchWriter {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchWriter.class);

    private BatchWriter() {
    }

    /**
     * Write a batch (.bat) file.
     *
     * @param fileName The name of the file.
     * @param content  The content of the file.
     * @param filePath The path of the file.
     */
    public static void writeFile(String fileName, String content, String filePath) {
        File batFile = new File(String.format("%s/%s.bat", filePath, fileName));

        try (FileWriter writer = new FileWriter(batFile)) {
            writer.write(content);

            AlertMaker.info(DIALOG_BATCH_TITLE,
                    DIALOG_BATCH_PASSED_HEADER,
                    DIALOG_BATCH_PASSED_CONTENT);
        } catch (IOException e) {
            AlertMaker.error(DIALOG_BATCH_TITLE,
                    DIALOG_BATCH_ERROR_HEADER,
                    e.getCause().getMessage());

            LOGGER.error(e.getMessage());
        }
    }
}
