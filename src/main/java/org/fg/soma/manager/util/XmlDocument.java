package org.fg.soma.manager.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * A wrapper utility class for {@link Document}.
 */
public final class XmlDocument {
    private static final Logger LOGGER = LoggerFactory.getLogger(XmlDocument.class);

    private Document document;

    private XmlDocument() {
    }

    /**
     * Creates a new xml document object from a desired xml file.<br>
     * If the file does not exist, a new xml file will be created physically.<br>
     * If the file exists, the data will be parsed into the xml document object.<br>
     *
     * @param file The desired file to read or write to. Needs to be an .xml file.
     */
    public XmlDocument(File file) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        dbFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

        DocumentBuilder dBuilder;

        try {
            dBuilder = dbFactory.newDocumentBuilder();

            Document doc;
            if (!file.exists()) {
                doc = dBuilder.newDocument();
                doc.setDocumentURI("file:/" + file.getAbsolutePath());
            } else {
                doc = dBuilder.parse(file);
                doc.getDocumentElement().normalize();
            }

            this.document = doc;
        } catch (ParserConfigurationException | IOException | SAXException e) {
            LOGGER.error(e.getMessage());

            throw new IllegalStateException("Document file does not exist or damaged.");
        }
    }

    /**
     * Get the {@link Document} object of this XmlDocument.
     *
     * @return Document object of this xml document.
     */
    public Document getDocument() {
        return this.document;
    }

    /**
     * Saves applies any change to the physical file and saves it.
     */
    public void save() {
        try {
            document.getDocumentElement().normalize();

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");

            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");


            DOMSource source = new DOMSource(document);
            String documentURI = document.getDocumentURI().replace("%20", " ");
            StreamResult result = new StreamResult(new File(documentURI.split("file:/")[1]));

            transformer.transform(source, result);
        } catch (TransformerException e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Returns the top element of the XML document.
     *
     * @return The root {@link Element} of the document.
     */
    public Element getRootElement() {
        return document.getDocumentElement();
    }

    /**
     * Get the first element found by its name.
     *
     * @param elementName The name of the element.
     * @return The firs {@link Element} found.
     */
    public Element getElementByName(String elementName) {
        return (Element) document.getElementsByTagName(elementName).item(0);
    }

    /**
     * Add a new element to the main document root.
     *
     * @param elementName The name of the element.
     */
    public void setRootElement(String elementName) {
        Element element = document.createElement(elementName);
        document.appendChild(element);
    }

    /**
     * Add element appended to another element.
     *
     * @param parentElement  The element to append the new element to.
     * @param newElementName The name of the new element.
     */
    public void addElement(Element parentElement, String newElementName) {
        parentElement.appendChild(document.createElement(newElementName));
    }


    /**
     * Set an XML document attribute value.
     *
     * @param attributeName  The attribute to set the value to.
     * @param attributeValue The value to set.
     */
    public void setRootAttribute(String attributeName, String attributeValue) {
        document.getDocumentElement().setAttribute(attributeName, attributeValue);
    }

    /**
     * Get an XML document attribute value.
     *
     * @param attributeName The attribute name to get the value from.
     * @return The value of the attribute.
     */
    public String getRootAttribute(String attributeName) {
        if (document != null) {
            return document.getDocumentElement().getAttribute(attributeName);
        } else {
            return "";
        }
    }
}
