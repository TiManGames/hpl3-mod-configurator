package org.fg.soma.manager.util.javafx;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

import static org.fg.soma.manager.util.application.GlobalConstants.USER_HOME;

/**
 * Utility class for handling JavaFX file choosers.
 */
public final class FileSelector {
    private FileSelector() {
    }

    /**
     * Displays a directory selector window for the user to select a folder.
     *
     * @param title            The title of the window.
     * @param initialDirectory The initial directory of the file chooser.
     * @param fileExtensions   The file extensions to filter.
     * @return The file represented as {@link File}.
     */
    public static File selectFile(String title, String initialDirectory, FileChooser.ExtensionFilter... fileExtensions) {
        String dir = initialDirectory.isEmpty() ? USER_HOME : initialDirectory;

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser.getExtensionFilters().addAll(fileExtensions);
        fileChooser.setInitialDirectory(new File(dir));

        return fileChooser.showOpenDialog(new Stage());
    }
}
