package org.fg.soma.manager.util.application;

public final class GlobalConstants {
    private GlobalConstants() {
    }

    //========================================GLOBALS=============================================

    public static final String USER_DIRECTORY = System.getProperty("user.dir");
    public static final String USER_HOME = System.getProperty("user.home");

    public static final String APPLICATION_TITLE_MAIN = "HPL3 Mod Manager";
    public static final String APPLICATION_TITLE_DEP = "Manage Mod Dependencies";
    public static final String APPLICATION_REPOSITORY_URL = "https://gitlab.com/TiManGames/hpl3-mod-configurator";
}
