package org.fg.soma.manager.util.application;

public final class SelectorsConstants {
    private SelectorsConstants() {
    }

    //========================================SELECTORS===========================================

    public static final String SELECT_GAME_DIRECTORY_TITLE = "Select game folder";

    public static final String SELECT_MOD_TITLE = "Select HPL3 mod entry file";
    public static final String SELECT_MOD_DESCRIPTION = "HPL mod entry file (*.hpc)";

    public static final String SELECT_MOD_IMAGE_TITLE = "Select image file";
    public static final String SELECT_MOD_IMAGE_DESCRIPTION = "Supported image files (*.png, *.jpg)";
}
