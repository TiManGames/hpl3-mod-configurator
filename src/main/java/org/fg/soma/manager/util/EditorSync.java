package org.fg.soma.manager.util;

import org.fg.soma.manager.models.config.WipModConfig;
import org.fg.soma.manager.models.mod.Mod;

import javax.swing.*;

/**
 * Utility class for handling the WIPMod configuration of the level editor.
 */
public final class EditorSync {
    private EditorSync() {
    }

    /**
     * Sync a mod with the level editor. That will make the level editor load any custom assets that may be present
     * within a mod.
     *
     * @param mod The mod to sync the editor to.
     */
    public static void syncTo(Mod mod) {
        String myDocumentsDir = new JFileChooser().getFileSystemView().getDefaultDirectory().toString();
        String engineDocumentsDir = myDocumentsDir + "\\HPL3";

        String modEntryFilePath = mod.getLocation().toString();
        String configFilePath = engineDocumentsDir + "\\WIPmod.cfg";

        WipModConfig configFile = new WipModConfig(mod, configFilePath);
        configFile.setModPath(modEntryFilePath);
    }
}
