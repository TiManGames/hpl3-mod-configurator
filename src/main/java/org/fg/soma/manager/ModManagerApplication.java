package org.fg.soma.manager;

import javafx.application.Application;
import javafx.stage.Stage;
import org.fg.soma.manager.config.ApplicationView;
import org.fg.soma.manager.config.StageManager;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;

@SpringBootApplication
public class ModManagerApplication extends Application {
    private ConfigurableApplicationContext springContext;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() {
        String[] args = getParameters().getRaw().toArray(new String[0]);

        springContext = new SpringApplicationBuilder(ModManagerApplication.class)
                .headless(false)
                .run(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        StageManager stageManager = springContext.getBean(StageManager.class, primaryStage);
        stageManager.openView(ApplicationView.MOD_MANAGER);
    }

    @Override
    public void stop() {
        springContext.stop();
    }
}
