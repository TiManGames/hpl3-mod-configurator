package org.fg.soma.manager.controllers;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import org.fg.soma.manager.models.ViewDataModel;
import org.fg.soma.manager.models.config.ApplicationConfig;
import org.fg.soma.manager.models.mod.Mod;
import org.fg.soma.manager.models.mod.ModProfilesManager;
import org.fg.soma.manager.util.javafx.AlertMaker;
import org.fg.soma.manager.util.javafx.FileSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import static org.fg.soma.manager.util.application.DialogConstants.*;
import static org.fg.soma.manager.util.application.SelectorsConstants.SELECT_MOD_DESCRIPTION;
import static org.fg.soma.manager.util.application.SelectorsConstants.SELECT_MOD_TITLE;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ModProfilesController implements Initializable {
    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private ModProfilesManager modProfilesManager;

    @Autowired
    private ViewDataModel viewDataModel;

    /////////////////////
    // BUTTONS
    ////////////////////

    @FXML
    public Button addModBtn;

    @FXML
    public Button newModBtn;

    @FXML
    public Button removeModBtn;

    /////////////////////
    // TABLES
    ////////////////////

    @FXML
    private TableView<Mod> modProfilesTable;

    @FXML
    private TableColumn<Mod, SimpleStringProperty> modNameCol;

    @FXML
    private TableColumn<Mod, SimpleStringProperty> modTypeCol;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Tables
        modProfilesTable.setRowFactory(fac -> {
            TableRow<Mod> row = new TableRow<>();
            row.setOnMouseClicked(this::onClickModProfileTableRow);

            return row;
        });

        modProfilesTable.itemsProperty().bindBidirectional(viewDataModel.modProfilesProperty());
        modProfilesTable.disableProperty().bind(viewDataModel.gamePathProperty().isEmpty());
        modProfilesTable.setItems(modProfilesManager.getModProfiles());

        viewDataModel.bindModProfilesTable(modProfilesTable);

        modNameCol.setCellValueFactory(new PropertyValueFactory<>("modName"));
        modTypeCol.setCellValueFactory(new PropertyValueFactory<>("modType"));

        // Data model
        viewDataModel.modProfilesDisableProperty().bind(modProfilesTable.disabledProperty());

        // Buttons
        addModBtn.disableProperty().bind(modProfilesTable.disabledProperty());
        newModBtn.disableProperty().bind(modProfilesTable.disabledProperty());
        removeModBtn.disableProperty().bind(
                Bindings.or(
                        modProfilesTable.disabledProperty(),
                        modProfilesTable.selectionModelProperty().get().selectedItemProperty().isNull()
                )
        );
    }

    @FXML
    private void onClickModProfileTableRow(MouseEvent mouseEvent) {
        Mod selectedMod = getSelectedMod();

        if (selectedMod != null) {
            modProfilesManager.setSelectedMod(selectedMod);

            viewDataModel.modNameProperty().setValue(selectedMod.getModName());
            viewDataModel.modAuthorProperty().setValue(selectedMod.getModAuthor());
            viewDataModel.modDescriptionProperty().setValue(selectedMod.getModDescription());
            viewDataModel.modThumbnailProperty().setValue(selectedMod.getModImageName());
            viewDataModel.modTypeProperty().setValue(selectedMod.getModType().getTypeName());
        }
    }

    @FXML
    private void onClickRemoveModBtn() {
        Mod selectedMod = getSelectedMod();

        modProfilesManager.removeMod(selectedMod);
        modProfilesTable.getItems().remove(selectedMod);

        viewDataModel.modNameProperty().setValue("");
        viewDataModel.modAuthorProperty().setValue("");
        viewDataModel.modDescriptionProperty().setValue("");
        viewDataModel.modThumbnailProperty().setValue("");
        viewDataModel.modTypeProperty().setValue("Mod Type");
    }

    @FXML
    private void onClickAddModBtn() {
        File modEntryFile = FileSelector.selectFile(SELECT_MOD_TITLE,
                applicationConfig.getGameFolderPath(),
                new FileChooser.ExtensionFilter(SELECT_MOD_DESCRIPTION, "*.hpc"));

        if (modEntryFile != null) {
            Mod mod = new Mod(modEntryFile.toPath());

            if (modProfilesManager.addMod(mod)) {
                modProfilesTable.setItems(modProfilesManager.getModProfiles());
            } else {
                AlertMaker.info(DIALOG_MOD_ADD_INFO_TITLE,
                        DIALOG_MOD_ADD_INFO_HEADER,
                        DIALOG_MOD_ADD_INFO_CONTENT);
            }
        }
    }

    @FXML
    private void onClickNewModBtn() {
        StringBuilder nameBuilder = new StringBuilder("New mod");
        ObservableList<Mod> items = modProfilesManager.getModProfiles();

        for (Mod mod : items) {
            if (mod.getModName().equals("New mod")) {
                nameBuilder.append("_").append(modProfilesManager.getModProfiles().size());

                break;
            }
        }

        Mod newMod = new Mod(nameBuilder.toString(),
                "Author",
                "Description",
                applicationConfig.getGameFolderPath());

        if (modProfilesManager.addMod(newMod)) {
            modProfilesTable.setItems(modProfilesManager.getModProfiles());
        } else {
            AlertMaker.info(DIALOG_MOD_ADD_INFO_TITLE,
                    DIALOG_MOD_ADD_INFO_HEADER,
                    DIALOG_MOD_ADD_INFO_CONTENT);
        }
    }

    private Mod getSelectedMod() {
        return modProfilesTable.getSelectionModel().getSelectedItem();
    }
}
