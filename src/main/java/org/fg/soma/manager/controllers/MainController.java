package org.fg.soma.manager.controllers;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import org.fg.soma.manager.config.ApplicationTheme;
import org.fg.soma.manager.config.StageManager;
import org.fg.soma.manager.models.config.ApplicationConfig;
import org.fg.soma.manager.util.javafx.AlertMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.fg.soma.manager.util.application.DialogConstants.*;
import static org.fg.soma.manager.util.application.GlobalConstants.APPLICATION_REPOSITORY_URL;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MainController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    @Autowired
    @Lazy
    private StageManager stageManager;

    @Autowired
    private ApplicationConfig applicationConfig;

    @FXML
    private void onClickAbout() {
        AlertMaker.info(DIALOG_ABOUT_TITLE,
                DIALOG_ABOUT_HEADER,
                DIALOG_ABOUT_CONTENT);
    }

    @FXML
    private void onClickViewRepository() {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(new URI(APPLICATION_REPOSITORY_URL));
            } catch (IOException | URISyntaxException e) {
                LOGGER.error(e.getMessage());
            }
        } else {
            AlertMaker.error(DIALOG_REPO_TITLE,
                    DIALOG_REPO_HEADER,
                    DIALOG_REPO_CONTENT);
        }
    }

    @FXML
    private void onClickColorThemeOption(Event event) {
        MenuItem item = (MenuItem) event.getSource();
        ApplicationTheme theme = Enum.valueOf(ApplicationTheme.class, item.getText().toUpperCase());

        stageManager.setTheme(theme);
        applicationConfig.setTheme(theme);
    }
}
