package org.fg.soma.manager.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import org.fg.soma.manager.models.ViewDataModel;
import org.fg.soma.manager.models.config.ApplicationConfig;
import org.fg.soma.manager.util.javafx.DirectorySelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import static org.fg.soma.manager.util.application.GlobalConstants.USER_DIRECTORY;
import static org.fg.soma.manager.util.application.SelectorsConstants.SELECT_GAME_DIRECTORY_TITLE;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class GameDirectoryController implements Initializable {
    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private ViewDataModel viewDataModel;

    @FXML
    private TextField gamePathField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gamePathField.textProperty().bindBidirectional(viewDataModel.gamePathProperty());

        String pathToSet;
        if (USER_DIRECTORY.contains("steamapps\\common\\SOMA")) {
            applicationConfig.setGameFolderPath(USER_DIRECTORY);

            pathToSet = USER_DIRECTORY;
        } else {
            pathToSet = applicationConfig.getGameFolderPath();
        }

        gamePathField.setText(pathToSet);
    }

    @FXML
    private void onClickChooseGameFolderBtn() {
        File directory = DirectorySelector.selectDirectory(SELECT_GAME_DIRECTORY_TITLE);
        String pathName = (directory != null ? directory.getAbsolutePath() : "");

        applicationConfig.setGameFolderPath(pathName);
        gamePathField.setText(pathName);
    }
}
