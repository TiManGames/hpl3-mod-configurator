package org.fg.soma.manager.controllers;

import javafx.beans.binding.Bindings;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import org.fg.soma.manager.config.ApplicationView;
import org.fg.soma.manager.config.StageManager;
import org.fg.soma.manager.models.ViewDataModel;
import org.fg.soma.manager.models.config.ApplicationConfig;
import org.fg.soma.manager.models.mod.Mod;
import org.fg.soma.manager.models.mod.ModDependency;
import org.fg.soma.manager.models.mod.ModProfilesManager;
import org.fg.soma.manager.models.mod.ModType;
import org.fg.soma.manager.util.EditorSync;
import org.fg.soma.manager.util.XmlDocument;
import org.fg.soma.manager.util.javafx.AlertMaker;
import org.fg.soma.manager.util.javafx.FileSelector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static org.fg.soma.manager.util.application.DialogConstants.*;
import static org.fg.soma.manager.util.application.SelectorsConstants.SELECT_MOD_IMAGE_DESCRIPTION;
import static org.fg.soma.manager.util.application.SelectorsConstants.SELECT_MOD_IMAGE_TITLE;

@Controller
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ModInformationController implements Initializable {
    @Lazy
    @Autowired
    private StageManager stageManager;

    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private ModProfilesManager modProfilesManager;

    @Autowired
    private ViewDataModel viewDataModel;

    @FXML
    private AnchorPane modInfoBottomPane;

    @FXML
    private GridPane modInfoGrid;

    /////////////////////
    // FIELDS
    ////////////////////

    @FXML
    private TextField modInfoNameField;

    @FXML
    private TextField modInfoAuthorField;

    @FXML
    private TextArea modDescriptionArea;

    @FXML
    private TextField modThumbnailField;

    /////////////////////
    // BUTTONS
    ////////////////////

    @FXML
    private MenuButton modTypeBtn;

    @FXML
    private Button applyBtn;

    @FXML
    private Button syncEditorBtn;

    @FXML
    private Button createDevBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        applyBtn.disableProperty().bind(
                Bindings.or(
                        modInfoNameField.textProperty().isEmpty(),
                        modInfoAuthorField.textProperty().isEmpty()
                )
        );

        modInfoGrid.disableProperty().bind(
                Bindings.or(
                        viewDataModel.modProfilesDisableProperty(),
                        viewDataModel.modProfilesTable().selectionModelProperty().get().selectedItemProperty().isNull()
                )
        );
        modInfoBottomPane.disableProperty().bind(modInfoGrid.disableProperty());

        syncEditorBtn.disableProperty().bind(modInfoBottomPane.disabledProperty());
        createDevBtn.disableProperty().bind(modInfoBottomPane.disabledProperty());

        modInfoNameField.textProperty().bindBidirectional(viewDataModel.modNameProperty());
        modInfoAuthorField.textProperty().bindBidirectional(viewDataModel.modAuthorProperty());
        modDescriptionArea.textProperty().bindBidirectional(viewDataModel.modDescriptionProperty());
        modThumbnailField.textProperty().bindBidirectional(viewDataModel.modThumbnailProperty());

        modTypeBtn.textProperty().bindBidirectional(viewDataModel.modTypeProperty());
        modTypeBtn.textProperty().setValue("Mod Type");
    }

    @FXML
    private void onClickDependenciesBtn() throws IOException {
        stageManager.openView(ApplicationView.MOD_DEPENDENCIES);
    }

    @FXML
    private void onClickChooseModImageBtn() {
        File image = FileSelector.selectFile(SELECT_MOD_IMAGE_TITLE,
                applicationConfig.getGameFolderPath(),
                new FileChooser.ExtensionFilter(SELECT_MOD_IMAGE_DESCRIPTION, "*.png", "*.jpg"));

        if (image != null) {
            Mod mod = modProfilesManager.getSelectedMod();
            mod.setModImage(image);

            modThumbnailField.setText(image.getName());
        }
    }

    @FXML
    private void onClickSyncEditorBtn() {
        Mod selectedMod = modProfilesManager.getSelectedMod();
        EditorSync.syncTo(selectedMod);
    }

    @FXML
    private void onClickCreateDevBtn() {
        Mod selectedMod = modProfilesManager.getSelectedMod();
        selectedMod.generateDevFile();
    }

    @FXML
    private void onSelectModTypeMenu(Event event) {
         MenuItem item = (MenuItem) event.getSource();
         modTypeBtn.textProperty().setValue(item.getText());
    }

    @FXML
    private void onClickModInfoApplyBtn() {
        Mod selectedMod = modProfilesManager.getSelectedMod();

        ModType type = Enum.valueOf(ModType.class, modTypeBtn.textProperty().getValue().toUpperCase());
        String name = modInfoNameField.getText();
        String author = modInfoAuthorField.getText();
        String description = modDescriptionArea.getText();
        String image = modThumbnailField.getText();
        String uid = selectedMod.getUID();

        StringBuilder dependencyNames = new StringBuilder();
        List<ModDependency> modDependencies = selectedMod.getDependencies();

        for (int i = 0; i < modDependencies.size(); i++) {
            dependencyNames.append(modDependencies.get(i).getDependencyName());

            if (i < modDependencies.size() - 1) {
                dependencyNames.append(",");
            }
        }

        // Update mod object
        Mod modFromManager = modProfilesManager.getModByPath(selectedMod.getLocation());
        modFromManager.setModName(name);
        modFromManager.setModAuthor(author);
        modFromManager.setDescription(description);
        modFromManager.setModImage(new File(String.format("%s/%s", selectedMod.getLocation().toString(), image)));
        modFromManager.setModType(type);

        // Update .hpc file
        XmlDocument modEntry;

        File modFolder = selectedMod.getLocation().toFile();

        if (!modFolder.getParentFile().exists()) {
            if (modFolder.getParentFile().mkdir()) {
                modEntry = new XmlDocument(new File(modFolder.getAbsolutePath()));

                modEntry.setRootElement("Content");
                modEntry.setRootAttribute("Version", "1.0.0");
                modEntry.setRootAttribute("InitCfg", "config/main_init.cfg");
            } else {
                AlertMaker.error(DIALOG_MOD_APPLY_ERROR_TITLE,
                        DIALOG_MOD_APPLY_ERROR_HEADER,
                        DIALOG_MOD_APPLY_ERROR_CONTENT);

                return;
            }
        } else {
            modEntry = new XmlDocument(new File(modFolder.getAbsolutePath()));
        }

        modEntry.setRootAttribute("Type", type.getTypeName());
        modEntry.setRootAttribute("Title", name);
        modEntry.setRootAttribute("Author", author);
        modEntry.setRootAttribute("Description", description);
        modEntry.setRootAttribute("LauncherPic", image);
        modEntry.setRootAttribute("UID", uid);
        modEntry.setRootAttribute("Dependencies", dependencyNames.toString());
        modEntry.save();

        // Update table
        viewDataModel.modProfilesTable().refresh();
    }
}
